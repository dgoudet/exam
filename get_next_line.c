int ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char *ft_strdup(char *str)
{
	int i;
	char *dup;

	i = 0;
	if ((dup = malloc(sizeof(char) * (ft_strlen(str) + 1))) == NULL)
		return (NULL);
	while (str[i])
	{
		dup[i] = str[i];
		i++;
	}
	dup[i] = '\0';
	return (dup);
}

char *ft_strjoin(char *s1, char *s2)
{
	int i;
	int j;
	char *s;

	i = 0;
	j = 0;
	if ((s = malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1))) == NULL)
		return (NULL);
	s = ft_strdup(s1);
	while (s1[i])
	{
		s[i] = s1[i];
		i++;
	}
	while (s2[j])
	{
		s[i] = s2[j];
		i++;
		j++;
	}
	s[i] = '\0';
	return (s);
}

char *ft_substr(char *str, int start, int size)
{
	char *s;
	int i;

	i = 0;
	if ((s = malloc(sizeof(char) * (ft_strlen(size) + 1))) == NULL)
		return (NULL);
	while (str[i] && i < size)
	{
		s[i] = str[start];
		start++;
		i++;
	}
	s[i] = '\0';
	return (s);
}

int is_line(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] == '\n')
			return (i);
		i++;
	}
	return (-1);
}

char *create_line(char *str, char **line)
{
	int i;

	i = 0;
	if (str[i] == '\0' || str[i] == '\n')
		return (*line = ft_strdup(""));
	return (*line = ft_substr(str, 0, is_line(str) - 1));
}

int gnl_return(char **line, char **str, int ret)
{
	char *temp;

	if (ret > 0)
	{
		*line = create_line(*str, line);
		temp = ft_strdup(*str);
		free(str);
		str = ft_substr(temp, is_line(temp) + 1, ft_strlen(temp) - is_line(temp));
		free(temp);
		return (1);
	}
	*line = ft_create_line(*str, line);
	str = NULL;
	free(str);
	return (0);
}

int get_next_line(char **line)
{
	int ret;
	char temp;
	char *buf;
	static char *str;

	ret = 1;
	if (!str)
	{
		if ((buf = malloc(sizeof(char) * 301)) == 0)
			return (-1);
		if ((ret = read(0, buf, 300)) < 0)
		{
			free(buf);
			return (-1);
		}
		buf[ret] = '\0';
		str = ft_strdup(buf);
		free(buf);
	}
	while (str && (is_line(str) == -1) && ret > 0)
	{
		if ((buf = malloc(sizeof(char) * 301)) == 0)
			return (-1);
		if ((ret = read(0, buf, 300)) < 0)
		{
			free(buf);
			return (-1);
		}
		buf[ret] = '\0';
		temp = ft_strdup(str);
		free(str);
		str = ft_strjoin(temp, buf);
		free(buf);
		free(temp);
	}
	return (gnl_return(line, &str, ret));
}
